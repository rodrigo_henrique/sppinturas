from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, DateField, SubmitField
from wtforms.validators import DataRequired

class PedidoForm(FlaskForm):

    # Condomínio
    nome_do_condominio = StringField('Nome do condomínio', validators=[DataRequired()])
    endereco = StringField('Endereço', validators=[DataRequired()])
    numero = IntegerField('Número', validators=[DataRequired()])
    CEP = StringField('CEP', validators=[DataRequired()])
    numero_de_predios = IntegerField('Número de prédios', validators=[DataRequired()])
    numero_de_andares_por_predio = IntegerField('Número de andares por prédio', validators=[DataRequired()])

    # Síndico
    nome_do_sindico = StringField('Nome do síndico', validators=[DataRequired()])
    celular_1_do_sindico = StringField('Celular 1', validators=[DataRequired()])
    celular_2_do_sindico = StringField('Celular 2')
    email_do_sindico = StringField('E-mail do síndico', validators=[DataRequired()])
    
    # Administradora
    nome_da_administradora = StringField('Nome da administradora', validators=[DataRequired()])
    telefone_1_da_administradora = StringField('Telefone 1', validators=[DataRequired()])
    telefone_2_da_administradora = StringField('Telefone 2')
    email_da_administradora = StringField('E-mail da administradora', validators=[DataRequired()])

    submit = SubmitField('Enviar')