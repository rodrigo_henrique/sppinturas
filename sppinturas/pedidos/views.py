# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
import datetime
# Models
from sppinturas.models.pedido import PedidoModel
from sppinturas.models.condominio import CondominioModel
from sppinturas.models.sindico import SindicoModel
from sppinturas.models.administradora import AdministradoraModel
# Forms
from sppinturas.pedidos.forms import PedidoForm

pedidos = Blueprint('pedidos', __name__)

# 1 - LISTAR PEDIDOS
@pedidos.route('/dashboard/listar_pedidos', methods=['GET'])
def listar_pedidos():

    pedidos = PedidoModel.find_all()

    datas_formatadas = []
    for pedido in pedidos:
        data_armazenada = pedido.data
        data_em_lista = data_armazenada.split('-')
        data_formatada = f'{data_em_lista[2]}/{data_em_lista[1]}/{data_em_lista[0]}'
        datas_formatadas.append(data_formatada)


    return render_template('lista_pedidos.html', pedidos=pedidos)


# 2 - CADASTRAR PEDIDO
@pedidos.route('/dashboard/adicionar_pedido', methods=['GET', 'POST'])
def adicionar_pedido():

    form = PedidoForm()

    if form.validate_on_submit():

        # Síndico
        nome_do_sindico = form.nome_do_sindico.data
        celular_1_do_sindico = form.celular_1_do_sindico.data
        celular_2_do_sindico = form.celular_2_do_sindico.data
        unwanted_chars = ['(', ')', ' ', '-']
        for c in unwanted_chars:
            celular_1_do_sindico = celular_1_do_sindico.replace(c, '')
            celular_2_do_sindico = celular_2_do_sindico.replace(c, '')
        email_do_sindico = form.email_do_sindico.data

        # Administradora
        nome_da_administradora = form.nome_da_administradora.data
        telefone_1_da_administradora = form.telefone_1_da_administradora.data
        telefone_2_da_administradora = form.telefone_2_da_administradora.data
        unwanted_chars = ['(', ')', ' ', '-']
        for c in unwanted_chars:
            telefone_1_da_administradora = telefone_1_da_administradora.replace(c, '')
            telefone_2_da_administradora = telefone_2_da_administradora.replace(c, '')
        email_da_administradora = form.email_da_administradora.data

        # Condomínio
        nome_do_condominio = form.nome_do_condominio.data
        endereco = form.endereco.data
        numero = form.numero.data
        CEP = form.CEP.data
        numero_de_predios = form.numero_de_predios.data
        numero_de_andares_por_predio = form.numero_de_andares_por_predio.data

        ####################################################################################################

        # 1 - Criando Síndico - POST

        novo_sindico = SindicoModel(nome_do_sindico, celular_1_do_sindico, celular_2_do_sindico, email_do_sindico)
        
        novo_sindico.add_to_db()
        novo_sindico.save_to_db()
        

        # 2 - Criando Administradora - POST

        nova_administradora = AdministradoraModel(nome_da_administradora, telefone_1_da_administradora, telefone_2_da_administradora, 
                                                  email_da_administradora)
        nova_administradora.add_to_db()
        nova_administradora.save_to_db()
        

        # 3 - Criando Condomínio - POST
        
        condominio = CondominioModel.find_by_condominio(nome_do_condominio, endereco, numero, CEP)
        if not condominio:
            novo_condominio = CondominioModel(nome_do_condominio, numero_de_predios, numero_de_andares_por_predio,
                                            endereco, numero, CEP, novo_sindico.id, nova_administradora.id)
            novo_condominio.add_to_db()
            novo_condominio.save_to_db()

        # 4 - Criando Pedido - POST

        data_do_cadastro_do_pedido = datetime.date.today()
        
        try:
            novo_pedido = PedidoModel(data_do_cadastro_do_pedido, 'Recebido', novo_condominio.id)
        except:
            novo_pedido = PedidoModel(data_do_cadastro_do_pedido, 'Recebido', condominio.id)

        novo_pedido.add_to_db()
        novo_pedido.save_to_db()
        
        return redirect(url_for('pedidos.listar_pedidos'))

    return render_template('formulario_pedido.html', form=form)

# 3 - LER OU ATUALIZAR PEDIDO
@pedidos.route('/dashboard/atualizar_pedido/<int:id_pedido>', methods=['GET', 'POST'])
def atualizar_pedido(id_pedido):

    pedido = PedidoModel.find_by_id(id_pedido)
    condominio = CondominioModel.find_by_id(pedido.id_condominio)
    sindico = SindicoModel.find_by_id(condominio.id_sindico)
    administradora = AdministradoraModel.find_by_id(condominio.id_administradora)

    form = PedidoForm()

    if form.validate_on_submit():

        # 1 - Atualizando Condomínio - UPDATE
        
        condominio.nome = form.nome_do_condominio.data
        condominio.endereco = form.endereco.data
        condominio.numero = form.numero.data
        condominio.CEP = form.CEP.data
        condominio.numero_de_predios = form.numero_de_predios.data
        condominio.numero_de_andares_por_predio = form.numero_de_andares_por_predio.data
        db.session.flush()
        db.session.commit()

        # 2 - Atualizando Síndico - UPDATE
        
        sindico.nome = form.nome_do_sindico.data
        sindico.nome = form.celular_1_do_sindico.data
        sindico.nome = form.celular_2_do_sindico.data
        sindico.nome = form.email_do_sindico.data
        db.session.flush()
        db.session.commit()
        
        # 3 - Atualizando Administradora - UPDATE
        
        administradora.nome = form.nome_da_administradora.data
        administradora.nome = form.telefone_1_da_administradora.data
        administradora.nome = form.telefone_2_da_administradora.data
        administradora.nome = form.email_da_administradora.data
        db.session.flush()
        db.session.commit()

        return redirect(url_for('pedidos.listar_pedidos'))

    elif request.method == 'GET':
        
        # Condomínio
        form.nome_do_condominio.data = condominio.nome
        form.endereco.data = condominio.endereco
        form.numero.data = condominio.numero
        form.CEP.data = condominio.CEP
        form.numero_de_predios.data = condominio.numero_de_predios
        form.numero_de_andares_por_predio.data = condominio.numero_de_andares_por_predio

        # Síndico
        form.nome_do_sindico.data = sindico.nome
        form.celular_1_do_sindico.data = sindico.celular_1
        form.celular_2_do_sindico.data = sindico.celular_2
        form.email_do_sindico.data = sindico.email
        
        # Administradora
        form.nome_da_administradora.data = administradora.nome
        form.telefone_1_da_administradora.data = administradora.telefone_1
        form.telefone_2_da_administradora.data = administradora.telefone_2
        form.email_da_administradora.data = administradora.email

    return render_template('formulario_pedido.html', form=form)

# 4 - DELETAR PEDIDO
@pedidos.route('/dashboard/deletar_pedido/<int:id_pedido>', methods=['POST'])
def deletar_pedido(id_pedido):

    pedido = PedidoModel.find_by_id(id_pedido)

    pedido.delete_from_db()

    return redirect(url_for('pedidos.listar_pedidos'))