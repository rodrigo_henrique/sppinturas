from sppinturas import db

class MaterialParaUtilizarModel(db.Model):

    __tablename__ = 'materiais_para_utilizar'

    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(50), nullable=False)
    quantidade = db.Column(db.Integer, nullable=False)
    id_orcamento = db.Column(db.Integer, db.ForeignKey('orcamentos.id'), nullable=False)

    def __init__(self, descricao, quantidade, id_orcamento):
        self.descricao = descricao
        self.quantidade = quantidade
        self.id_orcamento = id_orcamento

    def json(self):
        return {
            'id': self.id,
            'descricao': self.descricao,
            'quantidade': self.quantidade
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()
    
    @classmethod
    def find_by_fk(cls, id_orcamento):
        return cls.query.filter_by(id_orcamento=id_orcamento).all()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, descricao):
        return cls.query.filter_by(descricao=descricao).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()