from sppinturas import db

class AreaExtraParaTrabalharModel(db.Model):

    __tablename__ = 'areas_extras_para_trabalhar'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(30), nullable=False)
    valor_total = db.Column(db.Float, nullable=False)
    valor_pintura = db.Column(db.Float, nullable=True)
    valor_servicos = db.Column(db.Float, nullable=True)
    metragem = db.Column(db.Float, nullable=False)
    superficie = db.Column(db.String(50), nullable=True)
    id_orcamento_area_extra = db.Column(db.Integer, db.ForeignKey('orcamentos_areas_extras.id'), nullable=False)
    # Relationships
    area_extra_para_trabalhar_servico_para_fazer = db.relationship('AreaExtraParaTrabalharServicoParaFazerModel', backref='area_extra_para_trabalhar', uselist=True, cascade="all, delete-orphan", lazy=True)

    def __init__(self, nome, valor_total, valor_pintura, valor_servicos, metragem, superficie, id_orcamento_area_extra):
        self.nome = nome
        self.valor_total = valor_total
        self.valor_pintura = valor_pintura
        self.valor_servicos = valor_servicos
        self.metragem = metragem
        self.superficie = superficie
        self.id_orcamento_area_extra = id_orcamento_area_extra

    def json(self):
        servicos = []
        for servico in self.area_extra_para_trabalhar_servico_para_fazer:
            servicos.append(servico.servico_para_fazer_area_extra.json())
        return {
            'id': self.id,
            'nome': self.nome,
            'valor_total': self.valor_total,
            'valor_pintura': self.valor_pintura,
            'valor_servicos': self.valor_servicos,
            'metragem': self.metragem,
            'superficie': self.superficie,
            'servicos': servicos
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, nome):
        return cls.query.filter_by(nome=nome).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()