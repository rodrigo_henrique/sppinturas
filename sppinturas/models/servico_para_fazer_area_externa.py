from sppinturas import db

class ServicoParaFazerAreaExternaModel(db.Model):

    __tablename__ = 'servicos_para_fazer_area_externa'

    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(100), nullable=False)
    porcentagem = db.Column(db.Integer, nullable=False)
    valor = db.Column(db.Float, nullable=False)
    id_orcamento_area_externa = db.Column(db.Integer, db.ForeignKey('orcamentos_areas_externas.id'), nullable=False)

    def __init__(self, descricao, porcentagem, valor, id_orcamento_area_externa):
        self.descricao = descricao
        self.porcentagem = porcentagem
        self.valor = valor
        self.id_orcamento_area_externa = id_orcamento_area_externa

    def json(self):
        return {
            'descricao': self.descricao,
            'porcentagem': self.porcentagem,
            'valor': self.valor
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_fk(cls, id_orcamento_area_externa):
        return cls.query.filter_by(id_orcamento_area_externa=id_orcamento_area_externa).all()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, descricao):
        return cls.query.filter_by(descricao=descricao).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()