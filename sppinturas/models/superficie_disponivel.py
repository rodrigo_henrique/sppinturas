from sppinturas import db

class SuperficieDisponivelModel(db.Model):

    __tablename__ = 'superficies_disponiveis'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(50), nullable=False)
    fator_multiplicador = db.Column(db.Float, nullable=False)

    def __init__(self, nome, fator_multiplicador):
        self.nome = nome
        self.fator_multiplicador = fator_multiplicador

    def json(self):
        return {
            'nome': self.nome,
            'fator_multiplicador': self.fator_multiplicador
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, nome):
        return cls.query.filter_by(nome=nome).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()