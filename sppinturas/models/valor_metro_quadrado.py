from sppinturas import db

class ValorMetroQuadradoModel(db.Model):

    __tablename__ = 'valor_do_metro_quadrado'

    id = db.Column(db.Integer, primary_key=True)
    valor = db.Column(db.Float, nullable=False)

    def __init__(self, valor):
        self.valor = valor

    def json(self):
        return {
            'valor': self.valor
        }

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()