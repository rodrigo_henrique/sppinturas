from sppinturas import db

class OrcamentoAreaInternaModel(db.Model):

    __tablename__ = 'orcamentos_areas_internas'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    valor_total = db.Column(db.Float, nullable=False)
    valor_pintura = db.Column(db.Float, nullable=False)
    valor_servicos = db.Column(db.Float, nullable=False)
    metragem = db.Column(db.Float, nullable=False)
    superficie = db.Column(db.String(50), nullable=False)
    id_orcamento = db.Column(db.Integer, db.ForeignKey('orcamentos.id'), nullable=False)
    # Relationships
    servicos_para_fazer = db.relationship('ServicoParaFazerAreaInternaModel', backref='orcamento_area_interna', uselist=True, cascade="all, delete-orphan", lazy=True)

    def __init__(self, valor_total, valor_pintura, valor_servicos, metragem, superficie, id_orcamento):
        self.valor_total = valor_total
        self.valor_pintura = valor_pintura
        self.valor_servicos = valor_servicos
        self.metragem = metragem
        self.superficie = superficie
        self.id_orcamento = id_orcamento

    def json(self):
        servicos_para_fazer = []
        for servico_para_fazer in self.servicos_para_fazer:
            servicos_para_fazer.append(servico_para_fazer.json())
        return {
            'id': self.id,
            'valor_total': self.valor_total,
            'valor_pintura': self.valor_pintura,
            'valor_servicos': self.valor_servicos,
            'metragem': self.metragem,
            'superficie': self.superficie,
            'servicos': servicos_para_fazer
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()