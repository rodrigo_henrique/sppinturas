from sppinturas import db

class OrcamentoModel(db.Model):

    __tablename__ = 'orcamentos'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    valor_total = db.Column(db.Float, nullable=False)
    id_pedido = db.Column(db.Integer, db.ForeignKey('pedidos.id'), nullable=False)
    # Relationships
    orcamento_area_externa = db.relationship('OrcamentoAreaExternaModel', backref='orcamento', uselist=False, cascade="all, delete-orphan")
    orcamento_area_interna = db.relationship('OrcamentoAreaInternaModel', backref='orcamento', uselist=False, cascade="all, delete-orphan")
    orcamento_area_extra = db.relationship('OrcamentoAreaExtraModel', backref='orcamento', uselist=False, cascade="all, delete-orphan")
    materiais_para_utilizar = db.relationship('MaterialParaUtilizarModel', backref='orcamento', uselist=True, cascade="all, delete-orphan", lazy=True)

    def __init__(self, valor_total, id_pedido):
        self.valor_total = valor_total
        self.id_pedido = id_pedido

    def json(self):
        if self.orcamento_area_externa:
            orcamento_area_externa = self.orcamento_area_externa.json()
        else:
            orcamento_area_externa = None
        
        if self.orcamento_area_interna:
            orcamento_area_interna = self.orcamento_area_interna.json()
        else:
            orcamento_area_interna = None
        if self.orcamento_area_extra:
            orcamento_area_extra = self.orcamento_area_extra.json()
        else:
            orcamento_area_extra = None

        materiais = []
        for material in self.materiais_para_utilizar:
            materiais.append(material.json())
        
        return {
            'id': self.id,
            'id_pedido': self.id_pedido,
            'valor_total': self.valor_total,
            'orcamento_area_externa': orcamento_area_externa,
            'orcamento_area_interna': orcamento_area_interna,
            'orcamento_area_extra': orcamento_area_extra,
            'materiais': materiais,
            'pedido': self.pedido.json()
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()