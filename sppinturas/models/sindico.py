from sppinturas import db

class SindicoModel(db.Model):

    __tablename__ = 'sindicos'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(80), nullable=False)
    celular_1 = db.Column(db.String(11), nullable=False)
    celular_2 = db.Column(db.String(11), nullable=True)
    email = db.Column(db.String(50), nullable=False)
    condominio = db.relationship('CondominioModel', backref='sindico', uselist=False, lazy=True)

    def __init__(self, nome, celular_1, celular_2, email):
        self.nome = nome
        self.celular_1 = celular_1
        self.celular_2 = celular_2
        self.email = email

    def json(self):
        return {
            'nome': self.nome,
            'celular_1': self.celular_1,
            'celular_2': self.celular_2,
            'email': self.email
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()