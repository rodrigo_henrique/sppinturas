from sppinturas import db


class PedidoModel(db.Model):

    __tablename__ = 'pedidos'

    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(10), nullable=False)
    status = db.Column(db.String(8), nullable=False)
    id_condominio = db.Column(db.Integer, db.ForeignKey('condominios.id'), nullable=False)
    orcamentos = db.relationship('OrcamentoModel', backref='pedido', uselist=True, lazy=True)

    def __init__(self, data, status, id_condominio):
        self.data = data
        self.status = status
        self.id_condominio = id_condominio

    def json(self):
        return {
            'id': self.id,
            'data': self.data,
            'status': self.status,
            'condominio': {
                'nome': self.condominio.nome,
                'numero_de_predios': self.condominio.numero_de_predios,
                'numero_de_andares_por_predio': self.condominio.numero_de_andares_por_predio,
                'endereco': {
                    'logradouro': self.condominio.endereco,
                    'numero': self.condominio.numero,
                    'CEP': self.condominio.CEP
                },
                'sindico': {
                    'nome': self.condominio.sindico.nome,
                    'celular_1': self.condominio.sindico.celular_1,
                    'celular_2': self.condominio.sindico.celular_2,
                    'email': self.condominio.sindico.email
                },
                'administradora': {
                    'nome': self.condominio.administradora.nome,
                    'telefone_1': self.condominio.administradora.telefone_1,
                    'telefone_2': self.condominio.administradora.telefone_2,
                    'email': self.condominio.administradora.email
                }
            }
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_by_status(cls, status):
        return cls.query.filter_by(status=status)

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
