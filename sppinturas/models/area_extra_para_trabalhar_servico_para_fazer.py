from sppinturas import db

class AreaExtraParaTrabalharServicoParaFazerModel(db.Model):

    __tablename__ = 'areas_extras_para_trabalhar_servicos_para_fazer'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    id_area_extra_para_trabalhar = db.Column(db.Integer, db.ForeignKey('areas_extras_para_trabalhar.id'), nullable=False)
    id_servico_para_fazer_area_extra = db.Column(db.Integer, db.ForeignKey('servicos_para_fazer_area_extra.id'), nullable=False)
    id_orcamento_area_extra = db.Column(db.Integer, db.ForeignKey('orcamentos_areas_extras.id'), nullable=False)

    def __init__(self, id_area_extra_para_trabalhar, id_servico_para_fazer_area_extra, id_orcamento_area_extra):
        self.id_area_extra_para_trabalhar = id_area_extra_para_trabalhar
        self.id_servico_para_fazer_area_extra = id_servico_para_fazer_area_extra
        self.id_orcamento_area_extra = id_orcamento_area_extra

    def json(self):
        return {
            'id': self.id,
            'servico': self.servico_para_fazer_area_extra.json()
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, nome):
        return cls.query.filter_by(nome=nome).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()