from sppinturas import db

class ServicoParaFazerAreaExtraModel(db.Model):

    __tablename__ = 'servicos_para_fazer_area_extra'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(100), nullable=False)
    porcentagem = db.Column(db.Integer, nullable=False)
    valor = db.Column(db.Float, nullable=False)
    id_orcamento_area_extra = db.Column(db.Integer, db.ForeignKey('orcamentos_areas_extras.id'), nullable=False)
    # Relationships
    area_extra_para_trabalhar_servico_para_fazer = db.relationship('AreaExtraParaTrabalharServicoParaFazerModel', backref='servico_para_fazer_area_extra', uselist=False, cascade="all, delete-orphan", lazy=True)

    def __init__(self, descricao, porcentagem, valor, id_orcamento_area_extra):
        self.descricao = descricao
        self.porcentagem = porcentagem
        self.valor = valor
        self.id_orcamento_area_extra = id_orcamento_area_extra

    def json(self):
        return {
            'id': self.id,
            'descricao': self.descricao,
            'porcentagem': self.porcentagem,
            'valor': self.valor
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, descricao):
        return cls.query.filter_by(descricao=descricao).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()