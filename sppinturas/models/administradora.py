from sppinturas import db

class AdministradoraModel(db.Model):

    __tablename__ = 'administradoras'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(50), nullable=False)
    telefone_1 = db.Column(db.String(11), nullable=False)
    telefone_2 = db.Column(db.String(11), nullable=True)
    email = db.Column(db.String(50), nullable=False)
    condominio = db.relationship('CondominioModel', backref='administradora', uselist=True, lazy='dynamic')

    def __init__(self, nome, telefone_1, telefone_2, email):
        self.nome = nome
        self.telefone_1 = telefone_1
        self.telefone_2 = telefone_2
        self.email = email

    def json(self):
        return {
            'nome': self.nome,
            'telefone_1': self.telefone_1,
            'telefone_2': self.telefone_2,
            'email': self.email
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()