from sppinturas import db

class ServicoDisponivelModel(db.Model):

    __tablename__ = 'servicos_disponiveis'

    id = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(100), nullable=False)

    def __init__(self, descricao):
        self.descricao = descricao

    def json(self):
        return {
            'descricao': self.descricao
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, descricao):
        return cls.query.filter_by(descricao=descricao).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()