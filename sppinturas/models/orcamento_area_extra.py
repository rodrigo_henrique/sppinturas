from sppinturas import db

class OrcamentoAreaExtraModel(db.Model):

    __tablename__ = 'orcamentos_areas_extras'

    # Main Columns
    id = db.Column(db.Integer, primary_key=True)
    valor_total = db.Column(db.Float, nullable=False)
    id_orcamento = db.Column(db.Integer, db.ForeignKey('orcamentos.id'), nullable=False)
    # Relationships
    areas_extras_para_trabalhar_servicos_para_fazer = db.relationship('AreaExtraParaTrabalharServicoParaFazerModel', backref='orcamento_area_extra', uselist=True, cascade="all, delete-orphan", lazy=True)
    areas_extras_para_trabalhar = db.relationship('AreaExtraParaTrabalharModel', backref='orcamento_area_extra', uselist=True, cascade="all, delete-orphan", lazy=True)
    servicos_para_fazer_area_extra = db.relationship('ServicoParaFazerAreaExtraModel', backref='orcamento_area_extra', uselist=True, cascade="all, delete-orphan", lazy=True)

    def __init__(self, valor_total, id_orcamento):
        self.valor_total = valor_total
        self.id_orcamento = id_orcamento

    def json(self):
        areas = []
        for area_extra_para_trabalhar in self.areas_extras_para_trabalhar:
            areas.append(area_extra_para_trabalhar.json())
        return {
            'id': self.id,
            'valor_total': self.valor_total,
            'areas': areas
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()