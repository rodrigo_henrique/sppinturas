from sppinturas import db


class CondominioModel(db.Model):

    __tablename__ = 'condominios'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(50), nullable=False)
    numero_de_predios = db.Column(db.Integer, nullable=False)
    numero_de_andares_por_predio = db.Column(db.Integer, nullable=False)
    endereco = db.Column(db.String(50), nullable=False)
    numero = db.Column(db.Integer, nullable=False)
    CEP = db.Column(db.String(8), nullable=False)
    id_sindico = db.Column(db.Integer, db.ForeignKey('sindicos.id'), nullable=False)
    id_administradora = db.Column(db.Integer, db.ForeignKey('administradoras.id'), nullable=False)
    pedidos = db.relationship('PedidoModel', backref='condominio', uselist=True, lazy=True)

    def __init__(self, nome, numero_de_predios, numero_de_andares_por_predio, endereco, numero, CEP, id_sindico, id_administradora):
        self.nome = nome
        self.numero_de_predios = numero_de_predios
        self.numero_de_andares_por_predio = numero_de_andares_por_predio
        self.endereco = endereco
        self.numero = numero
        self.CEP = CEP
        self.id_sindico = id_sindico
        self.id_administradora = id_administradora

    def json(self):
        return {
            'id': self.id,
            'nome': self.nome,
            'numero_de_predios': self.numero_de_predios,
            'numero_de_andares_por_predio': self.numero_de_andares_por_predio,
            'endereco': self.endereco,
            'numero': self.numero,
            'CEP': self.CEP,
            'id_sindico': self.id_sindico,
            'id_administradora': self.id_administradora
        }

    @classmethod
    def find_by_id(cls, _id):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_condominio(cls, nome, endereco, numero, CEP):
        return cls.query.filter_by(nome=nome, endereco=endereco, numero=numero, CEP=CEP).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.flush()

    def save_to_db(self):
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
