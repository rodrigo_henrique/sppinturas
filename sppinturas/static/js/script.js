var fundoDoModal = document.getElementById('modal-background');
var modalExcluirRegistro = document.getElementById('container-delete-modal');
var modalBotaoExluir = document.getElementById('form-modal-exclude-button');

function openModal(obj, recurso) {
    fundoDoModal.style.visibility = 'visible';
    modalExcluirRegistro.style.visibility = 'visible';
    modalBotaoExluir.setAttribute('action', `/dashboard/${recurso}/${obj.id}`)
}

function closeModal() {
    fundoDoModal.style.visibility = 'hidden';
    modalExcluirRegistro.style.visibility = 'hidden';
}

var deleteButtonsList = document.querySelectorAll('.container-delete-button');

