from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restful import Api

app = Flask(__name__)

########################################################################################
##################################### DB SETUP #########################################
########################################################################################
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://hVCdxU3hLi:azAvwSEVwk@remotemysql.com:3306/hVCdxU3hLi'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.secret_key = 'mysecretkey'

db = SQLAlchemy(app)
Migrate(app,db)
########################################################################################
########################################################################################
########################################################################################



########################################################################################
#################################### API SETUP #########################################
########################################################################################
api = Api(app)

from sppinturas.resources.pedido import Pedido, ListaPedido
api.add_resource(Pedido, '/pedido', '/pedido/<int:_id>')
api.add_resource(ListaPedido, '/pedidos')

from sppinturas.resources.servico_disponivel import ServicoDisponivel, ListaServico
api.add_resource(ServicoDisponivel, '/servico_disponivel/<int:_id>', '/servico_disponivel')
api.add_resource(ListaServico, '/servicos_disponiveis')

from sppinturas.resources.superficie_disponivel import SuperficieDisponivel, ListaSuperficieDisponivel
api.add_resource(SuperficieDisponivel, '/superficie_disponivel', '/superficie_disponivel/<int:_id>')
api.add_resource(ListaSuperficieDisponivel, '/superficies_disponiveis')

from sppinturas.resources.area_extra_disponivel import AreaExtraDisponivel, ListaAreaExtraDisponivel
api.add_resource(AreaExtraDisponivel, '/area_extra_disponivel', '/area_extra_disponivel/<int:_id>')
api.add_resource(ListaAreaExtraDisponivel, '/areas_extras_disponiveis')

from sppinturas.resources.material_disponivel import MaterialDisponivel, ListaMaterialDisponivel
api.add_resource(MaterialDisponivel, '/material_disponivel', '/material_disponivel/<int:_id>')
api.add_resource(ListaMaterialDisponivel, '/materiais_disponiveis')

from sppinturas.resources.valor_metro_quadrado import ValorMetroQuadrado
api.add_resource(ValorMetroQuadrado, '/valor_do_metro_quadrado')

from sppinturas.resources.orcamento import Orcamento, ListaOrcamento, ListaOrcamentoView
api.add_resource(Orcamento, '/orcamento', '/orcamento/<int:_id>')
api.add_resource(ListaOrcamento, '/orcamentos')
api.add_resource(ListaOrcamentoView, '/dashboard/listar_orcamentos')
########################################################################################
########################################################################################
########################################################################################

########################################################################################
################################### VIEWS SETUP ########################################
########################################################################################
from sppinturas.core.views import core
from sppinturas.pedidos.views import pedidos
from sppinturas.superficies.views import superficies
from sppinturas.servicos.views import servicos
from sppinturas.area_extra.views import areas_extras
from sppinturas.materiais.views import materiais
from sppinturas.valor_metro_quadrado.views import valor_metro_quadrado
from sppinturas.usuarios.views import usuarios
from sppinturas.orcamentos.views import orcamentos

app.register_blueprint(core)
app.register_blueprint(pedidos)
app.register_blueprint(superficies)
app.register_blueprint(servicos)
app.register_blueprint(areas_extras)
app.register_blueprint(materiais)
app.register_blueprint(valor_metro_quadrado)
app.register_blueprint(usuarios)
app.register_blueprint(orcamentos)
########################################################################################
########################################################################################
########################################################################################