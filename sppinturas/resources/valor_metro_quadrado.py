# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.valor_metro_quadrado import ValorMetroQuadradoModel

####################################################################################################
#################### Criar, ler, atualizar ou deletar Valor do Metro Quadrado ######################             
####################################################################################################
class ValorMetroQuadrado(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados do valor do metro quadrado disponível no json
    parser.add_argument('valor', type=float, required=True, help='Este campo não pode ser nulo.')    
    
    ########################## Definindo funções de Valor do Metro Quadrado ########################
    # Função para ler valor do metro quadrado
    def get(self):
        valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()
        if valor_do_metro_quadrado:
            valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()[0]
            return {'valor_do_metro_quadrado': valor_do_metro_quadrado.json()}
        return {'mensagem': 'Valor do metro quadrado não disponível.'}, 404

    # Função para criar novo valor do metro quadrado
    def post(self):
        dados = ValorMetroQuadrado.parser.parse_args()

        valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()

        if not valor_do_metro_quadrado:
            novo_valor_do_metro_quadrado = ValorMetroQuadradoModel(dados['valor'])
            novo_valor_do_metro_quadrado.add_to_db()
            novo_valor_do_metro_quadrado.save_to_db()
            return {'mensagem': 'Valor do metro quadrado criado com sucesso!'}
        return {'mensagem': 'Um valor de metro quadrado já foi cadastrado.'}

    # Função para atualizar valor do metro quadrado
    def put(self):
        valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()
        if valor_do_metro_quadrado:
            dados = ValorMetroQuadrado.parser.parse_args()
            valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()[0]

            valor_do_metro_quadrado.valor = dados['valor']
            valor_do_metro_quadrado.add_to_db()
            valor_do_metro_quadrado.save_to_db()
            
            return {'valor_do_metro_quadrado_atualizado': valor_do_metro_quadrado.json()}
        return {'mensagem': 'Não há valor de metro quadrado cadastrado.'}, 404

    # Função para deletar valor do metro quadrado
    def delete(self):
        valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()
        if valor_do_metro_quadrado:
            valor_do_metro_quadrado = ValorMetroQuadradoModel.find_all()[0]
            valor_do_metro_quadrado.delete_from_db()
            return {'mensagem': 'Valor do metro quadrado deletado.'}
        return {'mensagem': 'Não há valor de metro quadrado cadastrado.'}, 404