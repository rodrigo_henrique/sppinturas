# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.superficie_disponivel import SuperficieDisponivelModel

####################################################################################################
##################### Criar, ler, atualizar ou deletar Superficie Disponível #######################             
####################################################################################################
class SuperficieDisponivel(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados da superfície disponível no json
    parser.add_argument('nome', required=True, help='Este campo não pode ser nulo.')
    parser.add_argument('fator_multiplicador', type=float, required=True, help='Este campo não pode ser nulo.')    
    
    ########################## Definindo funções de Superfície Disponível ##########################
    # Função para ler superfície disponível específica
    def get(self, _id):
        superficie_disponivel = SuperficieDisponivelModel.find_by_id(_id)
        if superficie_disponivel:
            return {'superficie_disponivel': superficie_disponivel.json()}
        return {'mensagem': 'Superfície não disponível.'}, 404

    # Função para criar nova superfície disponível
    def post(self):
        dados = SuperficieDisponivel.parser.parse_args()

        superficie = SuperficieDisponivelModel.find_by_name(dados['nome'])

        if not superficie:
            nova_superficie_disponivel = SuperficieDisponivelModel(dados['nome'], dados['fator_multiplicador'])
            nova_superficie_disponivel.add_to_db()
            nova_superficie_disponivel.save_to_db()
            return {'mensagem': 'Superfície criada com sucesso!'}
        return {'mensagem': 'Esta superfície já foi criada.'}

    # Função para atualizar superfície disponível
    def put(self, _id):
        superficie_disponivel = SuperficieDisponivelModel.find_by_id(_id)
        if superficie_disponivel:
            dados = SuperficieDisponivel.parser.parse_args()

            superficie_disponivel.nome = dados['nome']
            superficie_disponivel.fator_multiplicador = dados['fator_multiplicador']
            superficie_disponivel.add_to_db()
            superficie_disponivel.save_to_db()
            
            return {'superficie_disponivel_atualizada': superficie_disponivel.json()}
        return {'mensagem': 'Superfície não disponível.'}, 404

    # Função para deletar superfície disponível
    def delete(self, _id):
        superficie_disponivel = SuperficieDisponivelModel.find_by_id(_id)
        if superficie_disponivel:
            superficie_disponivel.delete_from_db()
            return {'mensagem': 'Superfície deletada.'}
        return {'mensagem': 'Superfície não disponível.'}, 404

####################################################################################################
############################# Listar todas as Superfícies Disponíveis ##############################             
####################################################################################################
class ListaSuperficieDisponivel(Resource):
    
    ######################## Definindo funções de ListaSuperficieDisponivel ########################
    # Função para listar todas as superfícies disponíveis
    def get(self):
        superficies_disponiveis = list(map(lambda x: x.json(), SuperficieDisponivelModel.find_all()))
        if superficies_disponiveis:
            return {'superficies_disponiveis': superficies_disponiveis}
        return {'mensagem': 'Não há superfícies disponiveis.'}, 404