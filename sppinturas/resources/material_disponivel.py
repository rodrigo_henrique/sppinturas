# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.material_disponivel import MaterialDisponivelModel

####################################################################################################
###################### Criar, ler, atualizar ou deletar Material Disponível ########################             
####################################################################################################
class MaterialDisponivel(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados do material disponível no json
    parser.add_argument('descricao', required=True, help='Este campo não pode ser nulo.')    
    
    ########################### Definindo funções de Material Disponível ###########################
    # Função para ler material disponível específico
    def get(self, _id):
        material_disponivel = MaterialDisponivelModel.find_by_id(_id)
        if material_disponivel:
            return {'material_disponivel': material_disponivel.json()}
        return {'mensagem': 'Material não disponível.'}, 404

    # Função para criar novo material disponível
    def post(self):
        dados = MaterialDisponivel.parser.parse_args()

        material = MaterialDisponivelModel.find_by_name(dados['descricao'])

        if not material:
            novo_material_disponivel = MaterialDisponivelModel(dados['descricao'])
            novo_material_disponivel.add_to_db()
            novo_material_disponivel.save_to_db()
            return {'mensagem': 'Material criado com sucesso!'}
        return {'mensagem': 'Este material já foi criado.'}

    # Função para atualizar material disponível
    def put(self, _id):
        material_disponivel = MaterialDisponivelModel.find_by_id(_id)
        if material_disponivel:
            dados = MaterialDisponivel.parser.parse_args()

            material_disponivel.descricao = dados['descricao']
            material_disponivel.add_to_db()
            material_disponivel.save_to_db()
            
            return {'material_disponivel_atualizado': material_disponivel.json()}
        return {'mensagem': 'Material não disponível.'}, 404

    # Função para deletar material disponível
    def delete(self, _id):
        material_disponivel = MaterialDisponivelModel.find_by_id(_id)
        if material_disponivel:
            material_disponivel.delete_from_db()
            return {'mensagem': 'Material deletado.'}
        return {'mensagem': 'Material não disponível.'}, 404

####################################################################################################
############################## Listar todos os Materiais Disponíveis ###############################             
####################################################################################################
class ListaMaterialDisponivel(Resource):
    
    ######################## Definindo funções de ListaMaterialDisponivel ########################
    # Função para listar todas os materiais disponíveis
    def get(self):
        materiais_disponiveis = list(map(lambda x: x.json(), MaterialDisponivelModel.find_all()))
        if materiais_disponiveis:
            return {'materiais_disponiveis': materiais_disponiveis}
        return {'mensagem': 'Não há materiais disponiveis.'}, 404