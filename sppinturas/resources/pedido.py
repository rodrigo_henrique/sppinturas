# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.pedido import PedidoModel
from sppinturas.models.condominio import CondominioModel
from sppinturas.models.sindico import SindicoModel
from sppinturas.models.administradora import AdministradoraModel 

####################################################################################################
############################## Criar, ler, atualizar ou deletar Pedido #############################             
####################################################################################################
class Pedido(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados do Pedido no json
    parser.add_argument('data', required=True, help='Este campo não pode ser nulo.')
    
    # Dados do Condomínio no json 
    parser.add_argument('condominio', type=dict, required=True, help='Este campo não pode ser nulo.')
    condominio_parser = reqparse.RequestParser()
    condominio_parser.add_argument('nome', required=True, location='condominio', help='Nome do condomínio não pode ser nulo.')
    condominio_parser.add_argument('numero_de_predios', type=int, required=True, location='condominio', help='Este campo não pode ser nulo.')
    condominio_parser.add_argument('numero_de_andares_por_predio', type=int, required=True, location='condominio', help='Este campo não pode ser nulo.')
    condominio_parser.add_argument('endereco', required=True, location='condominio', help='Este campo não pode ser nulo.')
    condominio_parser.add_argument('numero', required=True, type=int, location='condominio', help='Este campo não pode ser nulo.')
    condominio_parser.add_argument('CEP', required=True, location='condominio', help='Este campo não pode ser nulo.')

    # Dados do Síndico no json
    parser.add_argument('sindico', type=dict, required=True, help='Este campo não pode ser nulo.')
    sindico_parser = reqparse.RequestParser()
    sindico_parser.add_argument('nome', required=True, location='sindico', help='Este campo não pode ser nulo.')
    sindico_parser.add_argument('celular_1', required=True, location='sindico', help='Este campo não pode ser nulo.')
    sindico_parser.add_argument('celular_2', required=False, location='sindico', help='Este campo não pode ser nulo.')
    sindico_parser.add_argument('email', required=False, location='sindico', help='Este campo não pode ser nulo.')

    # Dados da Administradora no json
    parser.add_argument('administradora', type=dict, required=True, help='Este campo não pode ser nulo.')
    administradora_parser = reqparse.RequestParser()
    administradora_parser.add_argument('nome', required=True, location='administradora', help='Este campo não pode ser nulo.')
    administradora_parser.add_argument('telefone_1', required=True, location='administradora', help='Este campo não pode ser nulo.')
    administradora_parser.add_argument('telefone_2', required=False, location='administradora', help='Este campo não pode ser nulo.')
    administradora_parser.add_argument('email', required=False, location='administradora', help='Este campo não pode ser nulo.')    
    
    ################################## Definindo funções de Pedido #################################
    # Função para ler pedido específico 
    def get(self, _id):
        pedido = PedidoModel.find_by_id(_id)
        if pedido:
            return {'pedido': pedido.json()}
        return {'mensagem': 'Pedido não encontrado.'}, 404

    # Função para criar novo pedido
    def post(self):
        dados = Pedido.parser.parse_args()
        dados_condominio = Pedido.condominio_parser.parse_args(req=dados)
        dados_sindico = Pedido.sindico_parser.parse_args(req=dados)
        dados_administradora = Pedido.administradora_parser.parse_args(req=dados)

        novo_sindico = SindicoModel(dados_sindico['nome'], dados_sindico['celular_1'], dados_sindico['celular_2'], dados_sindico['email'])
        novo_sindico.add_to_db()
        novo_sindico.save_to_db()

        nova_administradora = AdministradoraModel(dados_administradora['nome'], dados_administradora['telefone_1'], dados_administradora['telefone_2'], 
                                                  dados_administradora['email'])
        nova_administradora.add_to_db()
        nova_administradora.save_to_db()
        
        condominio = CondominioModel.find_by_condominio(dados_condominio['nome'], dados_condominio['endereco'], dados_condominio['numero'], dados_condominio['CEP'])
        if not condominio:
            novo_condominio = CondominioModel(dados_condominio['nome'], dados_condominio['numero_de_predios'], dados_condominio['numero_de_andares_por_predio'],
                                            dados_condominio['endereco'], dados_condominio['numero'], dados_condominio['CEP'], novo_sindico.id, nova_administradora.id)
            novo_condominio.add_to_db()
            novo_condominio.save_to_db()
        
        try:
            novo_pedido = PedidoModel(dados['data'], 'Recebido', novo_condominio.id)
        except:
            novo_pedido = PedidoModel(dados['data'], 'Recebido', condominio.id)
        
        novo_pedido.add_to_db()
        novo_pedido.save_to_db()

        return {'mensagem': 'Pedido criado com sucesso!'}, 200

    # Função para atualizar pedido
    def put(self, _id):
        pedido = PedidoModel.find_by_id(_id)
        if pedido:
            dados = Pedido.parser.parse_args()
            dados_condominio = Pedido.condominio_parser.parse_args(req=dados)
            dados_sindico = Pedido.sindico_parser.parse_args(req=dados)
            dados_administradora = Pedido.administradora_parser.parse_args(req=dados)

            condominio = CondominioModel.find_by_id(pedido.id_condominio)
            sindico = SindicoModel.find_by_id(condominio.id_sindico)
            administradora = AdministradoraModel.find_by_id(condominio.id_administradora)

            pedido.data = dados['data']
            pedido.add_to_db()
            pedido.save_to_db()
            
            condominio.nome = dados_condominio['nome']
            condominio.numero_de_predios = dados_condominio['numero_de_predios']
            condominio.numero_de_andares_por_predio = dados_condominio['numero_de_andares_por_predio']
            condominio.endereco = dados_condominio['endereco']
            condominio.numero = dados_condominio['numero']
            condominio.CEP = dados_condominio['CEP']
            condominio.add_to_db()
            condominio.save_to_db()

            sindico.nome = dados_sindico['nome']
            sindico.celular_1 = dados_sindico['celular_1']
            sindico.celular_2 = dados_sindico['celular_2']
            sindico.email = dados_sindico['email']
            sindico.add_to_db()
            sindico.save_to_db()

            administradora.nome = dados_administradora['nome']
            administradora.telefone_1 = dados_administradora['telefone_1']
            administradora.telefone_2 = dados_administradora['telefone_2']
            administradora.email = dados_administradora['email']
            administradora.add_to_db()
            administradora.save_to_db()
            
            return {'pedido_atualizado': pedido.json()}
        return {'mensagem': 'Pedido não encontrado.'}, 404

    # Função para deletar pedido
    def delete(self, _id):
        pedido = PedidoModel.find_by_id(_id)
        if pedido:
            pedido.delete_from_db()
            return {'mensagem': 'Pedido deletado.'}
        return {'mensagem': 'Pedido não encontrado.'}, 404

####################################################################################################
####################################### Listar todos os pedidos ####################################             
####################################################################################################
class ListaPedido(Resource):
    
    ########################### Definindo funções do recurso ListaPedido ###########################
    # Função para listar todos os pedidos recebidos
    def get(self):
        pedidos = list(map(lambda x: x.json(), PedidoModel.find_by_status('Recebido')))
        if pedidos:
            return {'pedidos': pedidos}
        return {'mensagem': 'Não há pedidos.'}, 404