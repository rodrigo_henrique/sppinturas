# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.area_extra_disponivel import AreaExtraDisponivelModel

####################################################################################################
##################### Criar, ler, atualizar ou deletar Área Extra Disponível #######################             
####################################################################################################
class AreaExtraDisponivel(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados da área extra disponível no json
    parser.add_argument('nome', required=True, help='Este campo não pode ser nulo.')    
    
    ########################## Definindo funções de Área Extra Disponível ##########################
    # Função para ler área extra disponível específica
    def get(self, _id):
        area_extra_disponivel = AreaExtraDisponivelModel.find_by_id(_id)
        if area_extra_disponivel:
            return {'area_extra_disponivel': area_extra_disponivel.json()}
        return {'mensagem': 'Área extra não disponível.'}, 404

    # Função para criar nova área extra disponível
    def post(self):
        dados = AreaExtraDisponivel.parser.parse_args()

        area_extra_disponivel = AreaExtraDisponivelModel.find_by_name(dados['nome'])

        if not area_extra_disponivel:
            nova_area_extra_disponivel = AreaExtraDisponivelModel(dados['nome'])
            nova_area_extra_disponivel.add_to_db()
            nova_area_extra_disponivel.save_to_db()
            return {'mensagem': 'Área extra criada com sucesso!'}
        return {'mensagem': 'Esta área extra já foi criada.'}

    # Função para atualizar área extra disponível
    def put(self, _id):
        area_extra_disponivel = AreaExtraDisponivelModel.find_by_id(_id)
        if area_extra_disponivel:
            dados = AreaExtraDisponivel.parser.parse_args()

            area_extra_disponivel.nome = dados['nome']
            area_extra_disponivel.add_to_db()
            area_extra_disponivel.save_to_db()
            
            return {'area_extra_disponivel_atualizada': area_extra_disponivel.json()}
        return {'mensagem': 'Área extra não disponível.'}, 404

    # Função para deletar área extra disponível
    def delete(self, _id):
        area_extra_disponivel = AreaExtraDisponivelModel.find_by_id(_id)
        if area_extra_disponivel:
            area_extra_disponivel.delete_from_db()
            return {'mensagem': 'Área extra deletada.'}
        return {'mensagem': 'Área extra não disponível.'}, 404

####################################################################################################
############################# Listar todas as Áreas Extras Disponíveis #############################           
####################################################################################################
class ListaAreaExtraDisponivel(Resource):
    
    ########################### Definindo funções de ListaAreaDisponivel ###########################
    # Função para listar todas as áreas extras disponíveis
    def get(self):
        areas_extras_disponiveis = list(map(lambda x: x.json(), AreaExtraDisponivelModel.find_all()))
        if areas_extras_disponiveis:
            return {'areas_extras_disponiveis': areas_extras_disponiveis}
        return {'mensagem': 'Não há áreas extras disponiveis.'}, 404