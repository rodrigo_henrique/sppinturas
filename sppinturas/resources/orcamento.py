# Import de estruturas necessárias
from flask import Response, request, Request, Flask, make_response, render_template
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.pedido import PedidoModel
from sppinturas.models.orcamento import OrcamentoModel
from sppinturas.models.orcamento_area_externa import OrcamentoAreaExternaModel
from sppinturas.models.orcamento_area_interna import OrcamentoAreaInternaModel
from sppinturas.models.orcamento_area_extra import OrcamentoAreaExtraModel
from sppinturas.models.servico_para_fazer_area_externa import ServicoParaFazerAreaExternaModel
from sppinturas.models.servico_para_fazer_area_interna import ServicoParaFazerAreaInternaModel
from sppinturas.models.servico_para_fazer_area_extra import ServicoParaFazerAreaExtraModel
from sppinturas.models.area_extra_para_trabalhar import AreaExtraParaTrabalharModel
from sppinturas.models.area_extra_para_trabalhar_servico_para_fazer import AreaExtraParaTrabalharServicoParaFazerModel
from sppinturas.models.material_para_utilizar import MaterialParaUtilizarModel


########################################################################################################
############################ Criar, ler, atualizar ou deletar Orçamento ################################
########################################################################################################
class Orcamento(Resource):
    ####################################################################################################
    ################################# Definindo parâmetros do json #####################################

    # dados

    parser = reqparse.RequestParser()
    parser.add_argument('id_pedido', type=int, required=True,
                        help='Este campo não pode ser nulo.')
    parser.add_argument('valor_total', type=float,
                        required=True, help='Este campo não pode ser nulo.')
    parser.add_argument('orcamento', type=dict, required=True,
                        help='Este campo não pode ser nulo.')

    ####################################################################################################

    # dados_orcamento

    orcamento_parser = reqparse.RequestParser()
    orcamento_parser.add_argument(
        'orcamento_area_externa', type=dict, required=False, location='orcamento')
    orcamento_parser.add_argument(
        'orcamento_area_interna', type=dict, required=False, location='orcamento')
    orcamento_parser.add_argument(
        'orcamento_area_extra', type=dict, required=False, location='orcamento')
    orcamento_parser.add_argument(
        'materiais', type=dict, action='append', required=False, location='orcamento')

    ####################################################################################################

    # dados_orcamento_area_externa

    orcamento_area_externa_parser = reqparse.RequestParser()
    orcamento_area_externa_parser.add_argument(
        'valor_total', type=float, required=False, location='orcamento_area_externa')
    orcamento_area_externa_parser.add_argument(
        'detalhes_valor', type=dict, required=False, location='orcamento_area_externa')
    orcamento_area_externa_parser.add_argument(
        'metragem', type=float, required=False, location='orcamento_area_externa')
    orcamento_area_externa_parser.add_argument(
        'superficie', required=False, location='orcamento_area_externa')
    orcamento_area_externa_parser.add_argument(
        'servicos', type=dict, action='append', required=False, location='orcamento_area_externa')

    detalhes_valor_orcamento_area_externa_parser = reqparse.RequestParser()
    detalhes_valor_orcamento_area_externa_parser.add_argument(
        'valor_pintura', type=float, required=False, location='detalhes_valor')
    detalhes_valor_orcamento_area_externa_parser.add_argument(
        'valor_servicos', type=float, required=False, location='detalhes_valor')

    ####################################################################################################

    # dados_orcamento_area_interna

    orcamento_area_interna_parser = reqparse.RequestParser()
    orcamento_area_interna_parser.add_argument(
        'valor_total', type=float, required=False, location='orcamento_area_interna')
    orcamento_area_interna_parser.add_argument(
        'detalhes_valor', type=dict, required=False, location='orcamento_area_interna')
    orcamento_area_interna_parser.add_argument(
        'metragem', type=float, required=False, location='orcamento_area_interna')
    orcamento_area_interna_parser.add_argument(
        'superficie', required=False, location='orcamento_area_interna')
    orcamento_area_interna_parser.add_argument(
        'servicos', type=dict, action='append', required=False, location='orcamento_area_interna')

    detalhes_valor_orcamento_area_interna_parser = reqparse.RequestParser()
    detalhes_valor_orcamento_area_interna_parser.add_argument(
        'valor_pintura', type=float, required=False, location='detalhes_valor')
    detalhes_valor_orcamento_area_interna_parser.add_argument(
        'valor_servicos', type=float, required=False, location='detalhes_valor')

    ####################################################################################################

    # dados_orcamento_area_extra

    orcamento_area_extra_parser = reqparse.RequestParser()
    orcamento_area_extra_parser.add_argument(
        'valor_total', type=float, required=False, location='orcamento_area_extra')
    orcamento_area_extra_parser.add_argument(
        'areas', type=dict, action='append', required=False, location='orcamento_area_extra')

    ################################## Definindo funções de Orçamento ##################################
    # Função para ler orçamento específico
    def get(self, _id):
        orcamento = OrcamentoModel.find_by_id(_id)
        if orcamento:
            return {'orcamento': orcamento.json()}
        return {'mensagem': 'Orçamento não encontrado.'}, 404

    # Função para criar novo orçamento
    def post(self):
        dados = Orcamento.parser.parse_args()
        dados_orcamento = Orcamento.orcamento_parser.parse_args(req=dados)

        dados_orcamento_area_externa = Orcamento.orcamento_area_externa_parser.parse_args(
            req=dados_orcamento)
        dados_detalhes_valor_orcamento_area_externa = Orcamento.detalhes_valor_orcamento_area_externa_parser.parse_args(
            req=dados_orcamento_area_externa)

        dados_orcamento_area_interna = Orcamento.orcamento_area_interna_parser.parse_args(
            req=dados_orcamento)
        dados_detalhes_valor_orcamento_area_interna = Orcamento.detalhes_valor_orcamento_area_interna_parser.parse_args(
            req=dados_orcamento_area_interna)

        dados_orcamento_area_extra = Orcamento.orcamento_area_extra_parser.parse_args(
            req=dados_orcamento)

        ####################################################################################################

        # 1 - Criando orçamento - POST
        orcamento = OrcamentoModel(dados['valor_total'], dados['id_pedido'])

        orcamento.add_to_db()
        orcamento.save_to_db()

        ####################################################################################################

        # 2 - Criando orçamento de área externa - POST
        if dados_orcamento['orcamento_area_externa']:
            orcamento_area_externa = OrcamentoAreaExternaModel(
                dados_orcamento_area_externa['valor_total'],
                dados_detalhes_valor_orcamento_area_externa['valor_pintura'],
                dados_detalhes_valor_orcamento_area_externa['valor_servicos'],
                dados_orcamento_area_externa['metragem'],
                dados_orcamento_area_externa['superficie'],
                orcamento.id
            )
            orcamento_area_externa.add_to_db()
            orcamento_area_externa.save_to_db()

            # 2.1 - Criando serviços de área externa - POST
            servicos_area_externa = dados_orcamento_area_externa['servicos']
            for servico_area_externa in servicos_area_externa:
                servico_para_fazer_area_externa = ServicoParaFazerAreaExternaModel(
                    servico_area_externa['descricao'],
                    servico_area_externa['porcentagem'],
                    servico_area_externa['valor'],
                    orcamento_area_externa.id
                )
                servico_para_fazer_area_externa.add_to_db()
                servico_para_fazer_area_externa.save_to_db()

        ####################################################################################################

        # 3 - Criando orçamento de área interna - POST
        if dados_orcamento['orcamento_area_interna']:
            orcamento_area_interna = OrcamentoAreaInternaModel(
                dados_orcamento_area_interna['valor_total'],
                dados_detalhes_valor_orcamento_area_interna['valor_pintura'],
                dados_detalhes_valor_orcamento_area_interna['valor_servicos'],
                dados_orcamento_area_interna['metragem'],
                dados_orcamento_area_interna['superficie'],
                orcamento.id
            )
            orcamento_area_interna.add_to_db()
            orcamento_area_interna.save_to_db()

            # 3.1 - Criando serviços de área interna - POST
            servicos_area_interna = dados_orcamento_area_interna['servicos']
            for servico_area_interna in servicos_area_interna:
                servico_para_fazer_area_interna = ServicoParaFazerAreaInternaModel(
                    servico_area_interna['descricao'],
                    servico_area_interna['porcentagem'],
                    servico_area_interna['valor'],
                    orcamento_area_interna.id
                )
                servico_para_fazer_area_interna.add_to_db()
                servico_para_fazer_area_interna.save_to_db()

        ####################################################################################################

        # 4 - Criando orçamento da área extra - POST
        if dados_orcamento['orcamento_area_extra']:
            orcamento_area_extra = OrcamentoAreaExtraModel(
                dados_orcamento_area_extra['valor_total'], orcamento.id)

            orcamento_area_extra.add_to_db()
            orcamento_area_extra.save_to_db()

            # 4.1 - Criando orçamento de cada área extra - POST
            areas_extras = dados_orcamento_area_extra['areas']
            for area_extra in areas_extras:
                area_extra_para_trabalhar = AreaExtraParaTrabalharModel(
                    area_extra['nome'],
                    area_extra['valor_total'],
                    area_extra['detalhes_valor']['valor_pintura'],
                    area_extra['detalhes_valor']['valor_servicos'],
                    area_extra['metragem'],
                    area_extra['superficie'],
                    orcamento_area_extra.id
                )
                area_extra_para_trabalhar.add_to_db()
                area_extra_para_trabalhar.save_to_db()

                # 4.2 - Criando serviços de cada orçamento de área extra - POST
                servicos_area_extra = area_extra['servicos']
                for servico_area_extra in servicos_area_extra:
                    servico_para_fazer_area_extra = ServicoParaFazerAreaExtraModel(
                        servico_area_extra['descricao'],
                        servico_area_extra['porcentagem'],
                        servico_area_extra['valor'],
                        orcamento_area_extra.id
                    )
                    servico_para_fazer_area_extra.add_to_db()
                    servico_para_fazer_area_extra.save_to_db()

                    area_extra_para_trabalhar_servico_para_fazer = AreaExtraParaTrabalharServicoParaFazerModel(
                        area_extra_para_trabalhar.id,
                        servico_para_fazer_area_extra.id,
                        orcamento_area_extra.id
                    )
                    area_extra_para_trabalhar_servico_para_fazer.add_to_db()
                    area_extra_para_trabalhar_servico_para_fazer.save_to_db()

        ####################################################################################################

        # 5 - Criando materiais para utilizar - POST
        if dados_orcamento['materiais']:
            materiais = dados_orcamento['materiais']
            for material in materiais:
                material_para_utilizar = MaterialParaUtilizarModel(
                    material['material'], material['quantidade'], orcamento.id)

                material_para_utilizar.add_to_db()
                material_para_utilizar.save_to_db()

        return {'mensagem': 'Orçamento criado com sucesso!'}

    # Função para atualizar orçamento
    def put(self, _id):
        orcamento = OrcamentoModel.find_by_id(_id)
        if orcamento:
            dados = Orcamento.parser.parse_args()
            dados_orcamento = Orcamento.orcamento_parser.parse_args(req=dados)

            dados_orcamento_area_externa = Orcamento.orcamento_area_externa_parser.parse_args(
                req=dados_orcamento)
            dados_detalhes_valor_orcamento_area_externa = Orcamento.detalhes_valor_orcamento_area_externa_parser.parse_args(
                req=dados_orcamento_area_externa)

            dados_orcamento_area_interna = Orcamento.orcamento_area_interna_parser.parse_args(
                req=dados_orcamento)
            dados_detalhes_valor_orcamento_area_interna = Orcamento.detalhes_valor_orcamento_area_interna_parser.parse_args(
                req=dados_orcamento_area_interna)

            dados_orcamento_area_extra = Orcamento.orcamento_area_extra_parser.parse_args(
                req=dados_orcamento)

            ####################################################################################################

            # 1 - Atualizando orçamento de área externa - PUT
            orcamento_area_externa = OrcamentoAreaExternaModel.find_by_id(
                orcamento.id_orcamento_area_externa)
            if orcamento_area_externa:
                orcamento_area_externa.valor_total = dados_orcamento_area_externa['valor_total']
                orcamento_area_externa.valor_pintura = dados_detalhes_valor_orcamento_area_externa[
                    'valor_pintura']
                orcamento_area_externa.valor_servicos = dados_detalhes_valor_orcamento_area_externa[
                    'valor_servicos']
                orcamento_area_externa.metragem = dados_orcamento_area_externa['metragem']
                orcamento_area_externa.superficie = dados_orcamento_area_externa['superficie']
                orcamento_area_externa.add_to_db()
                orcamento_area_externa.save_to_db()

                # 1.1 - Atualizando serviços de área externa - PUT
                servicos_area_externa_antigos = ServicoParaFazerAreaExternaModel.find_by_fk(
                    orcamento_area_externa.id)
                servicos_area_externa_atualizados = dados_orcamento_area_externa['servicos']
                i = 0
                if servicos_area_externa_atualizados:
                    while i < len(servicos_area_externa_atualizados):
                        if servicos_area_externa_antigos:
                            if len(servicos_area_externa_antigos) > i:
                                servicos_area_externa_antigos[i].descricao = servicos_area_externa_atualizados[i]['descricao']
                                servicos_area_externa_antigos[i].porcentagem = servicos_area_externa_atualizados[i]['porcentagem']
                                servicos_area_externa_antigos[i].valor = servicos_area_externa_atualizados[i]['valor']
                                servicos_area_externa_antigos[i].add_to_db()
                                servicos_area_externa_antigos[i].save_to_db()
                            else:
                                servico_para_fazer_area_externa = ServicoParaFazerAreaExternaModel(
                                    servicos_area_externa_atualizados[i]['descricao'],
                                    servicos_area_externa_atualizados[i]['porcentagem'],
                                    servicos_area_externa_atualizados[i]['valor'],
                                    orcamento_area_externa.id
                                )
                                servico_para_fazer_area_externa.add_to_db()
                                servico_para_fazer_area_externa.save_to_db()
                        else:
                            servico_para_fazer_area_externa = ServicoParaFazerAreaExternaModel(
                                servicos_area_externa_atualizados[i]['descricao'],
                                servicos_area_externa_atualizados[i]['porcentagem'],
                                servicos_area_externa_atualizados[i]['valor'],
                                orcamento_area_externa.id
                            )
                            servico_para_fazer_area_externa.add_to_db()
                            servico_para_fazer_area_externa.save_to_db()
                        i = i + 1
                while i < len(servicos_area_externa_antigos):
                    if servicos_area_externa_antigos[i]:
                        servicos_area_externa_antigos[i].delete_from_db()
                    i = i + 1

            ####################################################################################################

            # 2 - Atualizando orçamento de área interna - PUT
            orcamento_area_interna = OrcamentoAreaInternaModel.find_by_id(
                orcamento.id_orcamento_area_interna)
            if orcamento_area_interna:
                orcamento_area_interna.valor_total = dados_orcamento_area_interna['valor_total']
                orcamento_area_interna.valor_pintura = dados_detalhes_valor_orcamento_area_interna[
                    'valor_pintura']
                orcamento_area_interna.valor_servicos = dados_detalhes_valor_orcamento_area_interna[
                    'valor_servicos']
                orcamento_area_interna.metragem = dados_orcamento_area_interna['metragem']
                orcamento_area_interna.superficie = dados_orcamento_area_interna['superficie']
                orcamento_area_interna.add_to_db()
                orcamento_area_interna.save_to_db()

                # 2.1 - Atualizando serviços de área interna - PUT
                servicos_area_interna_antigos = ServicoParaFazerAreaInternaModel.find_by_fk(
                    orcamento_area_interna.id)
                servicos_area_interna_atualizados = dados_orcamento_area_interna['servicos']
                i = 0
                if servicos_area_interna_atualizados:
                    while i < len(servicos_area_interna_atualizados):
                        if servicos_area_interna_antigos:
                            if len(servicos_area_interna_antigos) > i:
                                servicos_area_interna_antigos[i].descricao = servicos_area_interna_atualizados[i]['descricao']
                                servicos_area_interna_antigos[i].porcentagem = servicos_area_interna_atualizados[i]['porcentagem']
                                servicos_area_interna_antigos[i].valor = servicos_area_interna_atualizados[i]['valor']
                                servicos_area_interna_antigos[i].add_to_db()
                                servicos_area_interna_antigos[i].save_to_db()
                            else:
                                servico_para_fazer_area_interna = ServicoParaFazerAreaInternaModel(
                                    servicos_area_interna_atualizados[i]['descricao'],
                                    servicos_area_interna_atualizados[i]['porcentagem'],
                                    servicos_area_interna_atualizados[i]['valor'],
                                    orcamento_area_interna.id
                                )
                                servico_para_fazer_area_interna.add_to_db()
                                servico_para_fazer_area_interna.save_to_db()
                        else:
                            servico_para_fazer_area_interna = ServicoParaFazerAreaInternaModel(
                                servicos_area_interna_atualizados[i]['descricao'],
                                servicos_area_interna_atualizados[i]['porcentagem'],
                                servicos_area_interna_atualizados[i]['valor'],
                                orcamento_area_interna.id
                            )
                            servico_para_fazer_area_interna.add_to_db()
                            servico_para_fazer_area_interna.save_to_db()
                        i = i + 1
                while i < len(servicos_area_interna_antigos):
                    if servicos_area_interna_antigos[i]:
                        servicos_area_interna_antigos[i].delete_from_db()
                    i = i + 1

            ####################################################################################################

            return {'mensagem': 'Orçamento atualizado com sucesso!'}
        return {'mensagem': 'Orçamento não encontrado.'}, 404

    # Função para deletar orçamento
    def delete(self, _id):
        orcamento = OrcamentoModel.find_by_id(_id)
        if orcamento:
            orcamento.delete_from_db()
            return {'mensagem': 'Orçamento deletado.'}
        return {'mensagem': 'Orçamento não encontrado.'}, 404

####################################################################################################
#################################### Listar todos os orçamentos ####################################
####################################################################################################


class ListaOrcamento(Resource):

    ######################### Definindo funções do recurso ListaOrcamento ##########################
    # Função para listar todos os orçamentos feitos
    def get(self):
        orcamentos = list(map(lambda x: x.json(), OrcamentoModel.find_all()))
        if orcamentos:
            return {'orcamentos': orcamentos}
        return {'mensagem': 'Não há orçamentos.'}, 404


class ListaOrcamentoView(Resource):

    ######################### Definindo funções do recurso ListaOrcamento ##########################
    # Função para listar todos os orçamentos feitos
    def get(self):
        orcamentos = list(map(lambda x: x.json(), OrcamentoModel.find_all()))
        if orcamentos:
            headers = {'Content-Type': 'text/html'}
            return make_response(render_template('lista_orcamentos.html', orcamentos=orcamentos), 200, headers)
        return {'mensagem': 'Não há orçamentos.'}, 404