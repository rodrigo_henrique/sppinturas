# Import de estruturas necessárias
from flask_restful import Resource, reqparse
from sppinturas import db

# Import de models necessárias
from sppinturas.models.servico_disponivel import ServicoDisponivelModel

####################################################################################################
####################### Criar, ler, atualizar ou deletar Serviço Disponível ########################             
####################################################################################################
class ServicoDisponivel(Resource):
    ################################# Definido parâmetros do json ##################################
    parser = reqparse.RequestParser()

    # Dados do serviço disponível no json
    parser.add_argument('descricao', required=True, help='Este campo não pode ser nulo.')    
    
    ############################ Definindo funções de Serviço Disponível ###########################
    # Função para ler serviço disponível específico 
    def get(self, _id):
        servico_disponivel = ServicoDisponivelModel.find_by_id(_id)
        if servico_disponivel:
            return {'servico_disponivel': servico_disponivel.json()}
        return {'mensagem': 'Serviço não disponível.'}, 404

    # Função para criar novo serviço disponível
    def post(self):
        dados = ServicoDisponivel.parser.parse_args()

        servico = ServicoDisponivelModel.find_by_name(dados['descricao'])

        if not servico:
            novo_servico_disponivel = ServicoDisponivelModel(dados['descricao'])
            novo_servico_disponivel.add_to_db()
            novo_servico_disponivel.save_to_db()
            return {'mensagem': 'Serviço criado com sucesso!'}
        return {'mensagem': 'Este serviço já foi criado.'}

    # Função para atualizar serviço disponível
    def put(self, _id):
        servico_disponivel = ServicoDisponivelModel.find_by_id(_id)
        if servico_disponivel:
            dados = ServicoDisponivel.parser.parse_args()

            servico_disponivel.descricao = dados['descricao']
            servico_disponivel.add_to_db()
            servico_disponivel.save_to_db()
            
            return {'servico_disponivel_atualizado': servico_disponivel.json()}
        return {'mensagem': 'Serviço não disponível.'}, 404

    # Função para deletar serviço disponível
    def delete(self, _id):
        servico_disponivel = ServicoDisponivelModel.find_by_id(_id)
        if servico_disponivel:
            servico_disponivel.delete_from_db()
            return {'mensagem': 'Serviço deletado.'}
        return {'mensagem': 'Serviço não disponível.'}, 404

####################################################################################################
################################ Listar todos os Serviços Disponíveis ##############################             
####################################################################################################
class ListaServico(Resource):
    
    ############################## Definindo funções de ListaServico ###############################
    # Função para listar todos os serviços disponíveis
    def get(self):
        servicos_disponiveis = list(map(lambda x: x.json(), ServicoDisponivelModel.find_all()))
        if servicos_disponiveis:
            return {'servicos_disponiveis': servicos_disponiveis}
        return {'mensagem': 'Não há serviços disponiveis.'}, 404