from flask import Blueprint, render_template

core = Blueprint('core', __name__)

@core.route('/dashboard', methods=['GET'])
def index():
    return render_template('base.html')