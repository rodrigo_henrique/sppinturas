# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
from sppinturas.models.servico_disponivel import ServicoDisponivelModel
# Forms
from sppinturas.servicos.forms import ServicoForm

servicos = Blueprint('servicos', __name__)

# 1 - LISTAR SERVIÇOS

@servicos.route('/dashboard/listar_servicos', methods=['GET'])
def listar_servicos():

    servicos = ServicoDisponivelModel.find_all()

    return render_template('lista_servicos.html', servicos=servicos)


# 2 - CADASTRAR SERVIÇO
@servicos.route('/dashboard/adicionar_servico', methods=['GET', 'POST'])
def adicionar_servico():

    form = ServicoForm()

    if form.validate_on_submit():

        # Serviço
        descricao = form.descricao.data

        # 1 - Criando Serviço - POST

        novo_servico = ServicoDisponivelModel(descricao)

        novo_servico.add_to_db()
        novo_servico.save_to_db()

        return redirect(url_for('servicos.listar_servicos'))

    return render_template('formulario_servico.html', form=form, titulo='Novo Serviço')

# 3 - LER OU ATUALIZAR SERVIÇO
@servicos.route('/dashboard/atualizar_servico/<int:id_servico>', methods=['GET', 'POST'])
def atualizar_servico(id_servico):

    servico = ServicoDisponivelModel.find_by_id(id_servico)

    form = ServicoForm()

    if form.validate_on_submit():

        # 1 - Atualizando Serviço - UPDATE
        servico.descricao = form.descricao.data
        db.session.flush()
        db.session.commit()

        return redirect(url_for('servicos.listar_servicos'))

    elif request.method == 'GET':

        # Serviço
        form.descricao.data = servico.descricao

    return render_template('formulario_servico.html', form=form, titulo='Atualizar Serviços')

# 4 - DELETAR SERVIÇO
@servicos.route('/dashboard/deletar_servico/<int:id_servico>', methods=['POST'])
def deletar_servico(id_servico):

    servico = ServicoDisponivelModel.find_by_id(id_servico)

    servico.delete_from_db()

    return redirect(url_for('servicos.listar_servicos'))