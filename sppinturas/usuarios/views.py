# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
# from sppinturas.models.material_disponivel import MaterialDisponivelModel
# Forms
# from sppinturas.materiais.forms import MaterialForm

usuarios = Blueprint('usuarios', __name__)

# 1 - LISTAR USUÁRIOS
@usuarios.route('/admin', methods=['GET', 'POST'])
def login():

    return render_template('login.html')