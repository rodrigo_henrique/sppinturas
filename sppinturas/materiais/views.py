# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
from sppinturas.models.material_disponivel import MaterialDisponivelModel
# Forms
from sppinturas.materiais.forms import MaterialForm

materiais = Blueprint('materiais', __name__)

# 1 - LISTAR MATERIAIS
@materiais.route('/dashboard/listar_materiais', methods=['GET'])
def listar_materiais():

    materiais = MaterialDisponivelModel.find_all()

    return render_template('lista_materiais.html', materiais=materiais)


# 2 - CADASTRAR MATERIAL
@materiais.route('/dashboard/adicionar_material', methods=['GET', 'POST'])
def adicionar_material():

    form = MaterialForm()

    if form.validate_on_submit():

        # Material
        descricao = form.descricao.data

        # 1 - Criando Material - POST

        novo_material = MaterialDisponivelModel(descricao)

        novo_material.add_to_db()
        novo_material.save_to_db()

        return redirect(url_for('materiais.listar_materiais'))

    return render_template('formulario_material.html', form=form, titulo='Novo Material')

# 3 - LER OU ATUALIZAR MATERIAL
@materiais.route('/dashboard/atualizar_material/<int:id_material>', methods=['GET', 'POST'])
def atualizar_material(id_material):

    material = MaterialDisponivelModel.find_by_id(id_material)

    form = MaterialForm()

    if form.validate_on_submit():

        # 1 - Atualizando Material - UPDATE
        material.descricao = form.descricao.data
        db.session.flush()
        db.session.commit()

        return redirect(url_for('materiais.listar_materiais'))

    elif request.method == 'GET':

        # Material
        form.descricao.data = material.descricao

    return render_template('formulario_material.html', form=form, titulo='Atualizar Material')

# 4 - DELETAR MATERIAL
@materiais.route('/dashboard/deletar_material/<int:id_material>', methods=['POST'])
def deletar_material(id_material):

    material = MaterialDisponivelModel.find_by_id(id_material)

    material.delete_from_db()

    return redirect(url_for('materiais.listar_materiais'))