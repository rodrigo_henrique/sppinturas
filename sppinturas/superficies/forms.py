from flask_wtf import FlaskForm
from wtforms import StringField, FloatField, SubmitField
from wtforms.validators import DataRequired

class SuperficieForm(FlaskForm):

    # Superfície
    nome = StringField('Nome', validators=[DataRequired()])
    fator_multiplicador = FloatField('Fator multiplicador', validators=[DataRequired()])

    submit = SubmitField('Enviar')