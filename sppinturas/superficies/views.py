# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
from sppinturas.models.superficie_disponivel import SuperficieDisponivelModel
# Forms
from sppinturas.superficies.forms import SuperficieForm

superficies = Blueprint('superficies', __name__)

# 1 - LISTAR SUPERFICIES

@superficies.route('/dashboard/listar_superficies', methods=['GET'])
def listar_superficies():

    superficies = SuperficieDisponivelModel.find_all()

    return render_template('lista_superficies.html', superficies=superficies)


# 2 - CADASTRAR SUPERFICIE
@superficies.route('/dashboard/adicionar_superficie', methods=['GET', 'POST'])
def adicionar_superficie():

    form = SuperficieForm()

    if form.validate_on_submit():

        # Superficie
        nome = form.nome.data
        fator_multiplicador = form.fator_multiplicador.data

        # 1 - Criando Superfície - POST

        nova_superficie = SuperficieDisponivelModel(nome, fator_multiplicador)

        nova_superficie.add_to_db()
        nova_superficie.save_to_db()

        return redirect(url_for('superficies.listar_superficies'))

    return render_template('formulario_superficie.html', form=form, titulo='Nova Superfície')

# 3 - LER OU ATUALIZAR SUPERFICIE
@superficies.route('/dashboard/atualizar_superficie/<int:id_superficie>', methods=['GET', 'POST'])
def atualizar_superficie(id_superficie):

    superficie = SuperficieDisponivelModel.find_by_id(id_superficie)

    form = SuperficieForm()

    if form.validate_on_submit():

        # 1 - Atualizando Superfície - UPDATE
        superficie.nome = form.nome.data
        superficie.fator_multiplicador = form.fator_multiplicador.data
        db.session.flush()
        db.session.commit()

        return redirect(url_for('superficies.listar_superficies'))

    elif request.method == 'GET':

        # Superfície
        form.nome.data = superficie.nome
        form.fator_multiplicador.data = superficie.fator_multiplicador

    return render_template('formulario_superficie.html', form=form, titulo='Atualizar Superfície')

# 4 - DELETAR SUPERFÍCIE
@superficies.route('/dashboard/deletar_superficie/<int:id_superficie>', methods=['POST'])
def deletar_superficie(id_superficie):

    superficie = SuperficieDisponivelModel.find_by_id(id_superficie)

    superficie.delete_from_db()

    return redirect(url_for('superficies.listar_superficies'))