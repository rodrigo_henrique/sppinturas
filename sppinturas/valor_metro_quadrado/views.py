# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
from sppinturas.models.valor_metro_quadrado import ValorMetroQuadradoModel
# Forms
from sppinturas.valor_metro_quadrado.forms import ValorMetroQuadradoForm

valor_metro_quadrado = Blueprint('valor_metro_quadrado', __name__)

# 1 - VER, CADASTRAR OU ATUALIZAR VALOR DO METRO QUADRADO
@valor_metro_quadrado.route('/dashboard/valor_metro_quadrado', methods=['GET', 'POST'])
def manter_valor_metro_quadrado():

    form = ValorMetroQuadradoForm()
    if request.method == 'GET':
        try:
            lista_valores_registrados = ValorMetroQuadradoModel.find_all()
            valor_registrado = lista_valores_registrados[0].valor
            form.valor.data = valor_registrado
            return render_template('valor_metro_quadrado.html', form=form)
        except:
            return render_template('valor_metro_quadrado.html', form=form)
    elif request.method == 'POST':
        try:
            lista_valores_registrados = ValorMetroQuadradoModel.find_all()
            objeto_valor_registrado = lista_valores_registrados[0]
            valor_atualizado = form.valor.data
            objeto_valor_registrado.valor = valor_atualizado
            db.session.flush()
            db.session.commit()
            return redirect(url_for('valor_metro_quadrado.manter_valor_metro_quadrado'))
        except:
            print('Não atualizou!')
            return redirect(url_for('valor_metro_quadrado.manter_valor_metro_quadrado'))