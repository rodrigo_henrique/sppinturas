from flask_wtf import FlaskForm
from wtforms import FloatField, SubmitField
from wtforms.validators import DataRequired

class ValorMetroQuadradoForm(FlaskForm):

    # Valor do metro quadrado
    valor = FloatField('Valor atual', validators=[DataRequired()])

    submit = SubmitField('Enviar')