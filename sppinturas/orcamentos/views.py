# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
import datetime
# Models
from sppinturas.models.orcamento import OrcamentoModel
from sppinturas.models.orcamento_area_externa import OrcamentoAreaExternaModel
from sppinturas.models.orcamento_area_interna import OrcamentoAreaInternaModel
# Forms
from sppinturas.orcamentos.forms import OrcamentoForm

orcamentos = Blueprint('orcamentos', __name__)

# 1 - LER OU ATUALIZAR ORÇAMENTO

@orcamentos.route('/dashboard/atualizar_orcamento/<int:id_orcamento>', methods=['GET', 'POST'])
def atualizar_orcamento(id_orcamento):

    orcamento = OrcamentoModel.find_by_id(id_orcamento)
    orcamento_area_externa = OrcamentoAreaExternaModel.find_by_id(orcamento.orcamento_area_externa.id)
    orcamento_area_interna = OrcamentoAreaInternaModel.find_by_id(orcamento.orcamento_area_interna.id)

    form = OrcamentoForm()

    # Orçamento
    form.valor_total.data = orcamento.valor_total
    form.nome_do_condominio.data = orcamento.pedido.condominio.nome
    form.endereco.data = orcamento.pedido.condominio.endereco
    form.numero.data = orcamento.pedido.condominio.numero

    # Orçamento Área Externa
    form.valor_total_orcamento_area_externa.data = orcamento_area_externa.valor_total
    form.valor_pintura_orcamento_area_externa.data = orcamento_area_externa.valor_pintura
    form.valor_servicos_orcamento_area_externa.data = orcamento_area_externa.valor_servicos
    form.metragem_orcamento_area_externa.data = orcamento_area_externa.metragem
    form.superficie_orcamento_area_externa.data = orcamento_area_externa.superficie

    # Orçamento Área Interna
    form.valor_total_orcamento_area_interna.data = orcamento_area_interna.valor_total
    form.valor_pintura_orcamento_area_interna.data = orcamento_area_interna.valor_pintura
    form.valor_servicos_orcamento_area_interna.data = orcamento_area_interna.valor_servicos
    form.metragem_orcamento_area_interna.data = orcamento_area_interna.metragem
    form.superficie_orcamento_area_interna.data = orcamento_area_interna.superficie

    return render_template('formulario_orcamento.html', form=form)

# 4 - DELETAR PEDIDO
@orcamentos.route('/dashboard/deletar_orcamento/<int:id_orcamento>', methods=['POST'])
def deletar_pedido(id_orcamento):

    orcamento = OrcamentoModel.find_by_id(id_orcamento)

    orcamento.delete_from_db()

    return redirect(url_for('listaorcamentoview'))