from flask_wtf import FlaskForm
from wtforms import StringField, FloatField, IntegerField, SubmitField
from wtforms.validators import DataRequired

class OrcamentoForm(FlaskForm):

    # Orçamento
    valor_total = FloatField('Valor total', validators=[DataRequired()])
    nome_do_condominio = StringField('Condomínio', validators=[DataRequired()])
    endereco = StringField('Endereço', validators=[DataRequired()])
    numero = IntegerField('Número', validators=[DataRequired()])

    # Orçamento Área Externa
    valor_total_orcamento_area_externa = FloatField('Valor total', validators=[DataRequired()])
    valor_pintura_orcamento_area_externa = FloatField('Valor da pintura', validators=[DataRequired()])
    valor_servicos_orcamento_area_externa = FloatField('Valor dos serviços', validators=[DataRequired()])
    metragem_orcamento_area_externa = FloatField('Metragem', validators=[DataRequired()])
    superficie_orcamento_area_externa = StringField('Superfície', validators=[DataRequired()])

    # Orçamento Área Interna
    valor_total_orcamento_area_interna = FloatField('Valor total', validators=[DataRequired()])
    valor_pintura_orcamento_area_interna = FloatField('Valor da pintura', validators=[DataRequired()])
    valor_servicos_orcamento_area_interna = FloatField('Valor dos serviços', validators=[DataRequired()])
    metragem_orcamento_area_interna = FloatField('Metragem', validators=[DataRequired()])
    superficie_orcamento_area_interna = StringField('Superfície', validators=[DataRequired()])