# Dependencies
from flask import Blueprint, render_template, url_for, redirect, request
from sppinturas import db
# Models
from sppinturas.models.area_extra_disponivel import AreaExtraDisponivelModel
# Forms
from sppinturas.area_extra.forms import AreaExtraForm

areas_extras = Blueprint('areas_extras', __name__)

# 1 - LISTAR ÁREAS EXTRAS

@areas_extras.route('/dashboard/listar_areas_extras', methods=['GET'])
def listar_areas_extras():

    areas_extras = AreaExtraDisponivelModel.find_all()

    return render_template('lista_areas_extras.html', areas_extras=areas_extras)


# 2 - CADASTRAR ÁREA EXTRA
@areas_extras.route('/dashboard/adicionar_area_extra', methods=['GET', 'POST'])
def adicionar_area_extra():

    form = AreaExtraForm()

    if form.validate_on_submit():

        # Área Extra
        nome = form.nome.data

        # 1 - Criando Área Extra - POST

        nova_area_extra = AreaExtraDisponivelModel(nome)

        nova_area_extra.add_to_db()
        nova_area_extra.save_to_db()

        return redirect(url_for('areas_extras.listar_areas_extras'))

    return render_template('formulario_area_extra.html', form=form, titulo='Nova Área Extra')

# 3 - LER OU ATUALIZAR ÁREA EXTRA
@areas_extras.route('/dashboard/atualizar_area_extra/<int:id_area_extra>', methods=['GET', 'POST'])
def atualizar_area_extra(id_area_extra):

    area_extra = AreaExtraDisponivelModel.find_by_id(id_area_extra)

    form = AreaExtraForm()

    if form.validate_on_submit():

        # 1 - Atualizando Área Extra - UPDATE
        area_extra.nome = form.nome.data
        db.session.flush()
        db.session.commit()

        return redirect(url_for('areas_extras.listar_areas_extras'))

    elif request.method == 'GET':

        # Área Extra
        form.nome.data = area_extra.nome

    return render_template('formulario_area_extra.html', form=form, titulo='Atualizar Área Extra')

# 4 - DELETAR ÁREA EXTRA
@areas_extras.route('/dashboard/deletar_area_extra/<int:id_area_extra>', methods=['POST'])
def deletar_area_extra(id_area_extra):

    area_extra = AreaExtraDisponivelModel.find_by_id(id_area_extra)

    area_extra.delete_from_db()

    return redirect(url_for('areas_extras.listar_areas_extras'))