from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class AreaExtraForm(FlaskForm):

    # Área Extra
    nome = StringField('Nome', validators=[DataRequired()])

    submit = SubmitField('Enviar')